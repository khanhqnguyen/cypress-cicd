/// <reference types="cypress" />

describe('example to-do app', () => {
    beforeEach(() => {
      cy.visit('https://portal.test.kms-connect.com/login/')
    })
    it('Sign up successfully', ()=>{
      cy.get('input[name="username"]').type("khanhqnguyen@kms-technology.com")
      cy.get('input[name="password"]').type("Kazamidori2707@")
      cy.get("[type='submit']").click()
      cy.contains("Login successfully!").should('be.visible')
      cy.url().should('include','/dashboard')
    })
    it('Create an app successfully!', ()=>{
        cy.contains("App").click()
        cy.contains("Create App").click()
        cy.get(".ant-modal-title").should('be.visible')
        cy.get('input[name="name"]').type("Test App 2")
        cy.get('textarea[name="description"]').type("test")
        cy.get("[type='submit']").click()
        cy.contains('Test App 2').should('be.visible')
    })
    it.skip('Config App', ()=>{
        cy.contains('Test App 2').click()
        cy.contains('Provider').click()
        cy.get(".ant-select-selection-item").click()
    })
  })
  