/// <reference types="cypress" />

describe('example to-do app', () => {
  beforeEach(() => {
    cy.visit('https://portal.test.kms-connect.com/login/')
  })
  it('Sign up successfully', ()=>{
    cy.contains("Create an Account").click()
    cy.get('input[name="firstName"]').type("Khanh")
    cy.get('input[name="lastName"]').type("Testing")
    cy.get('input[name="email"]').type("ngquockhanh0705.it@gmail.com")
    cy.get('input[name="password"]').type("Khanhtest123@")
    cy.get('input[name="confirmedPassword"]').type("Khanhtest123@")
    cy.get('input[type="checkbox"]').click()
    cy.get("[type='submit']").click()
  })
})
